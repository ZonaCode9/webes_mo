<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $file = fopen("password.txt", "r");
        $found = false;

        if ($file) {
            while (($line = fgets($file)) !== false) {

                $chars = str_split($line);
                $nums = array(5, -14, 31, -9, 3);
                $i = 0;

                $end = array();

                foreach($chars as $c){
                    if(ord($c) != 10) {
                        $end[] = chr(ord($c) - $nums[$i % 5]);
                    }
                    $i++;
                }

                $ln = implode("",$end);
                $pieces = explode("*", $ln);

                if($pieces[0] == $username) {
                    $found = true;
                    if($pieces[1] != $password) {
                        header("Location: main.php?error=2");
                        exit();
                    }
                    else {
                        $dictionary = [
                            "piros" => "red",
                            "zold" => "green",
                            "sarga" => "yellow",
                            "kek" => "blue",
                            "fekete" => "black",
                            "feher" => "white"
                        ];

                        $conn = new mysqli("sql303.epizy.com", "epiz_34156627", "thdvejpBIO05", "epiz_34156627_adatok") or die("Connection error!");

                        $sql = "SELECT Titkos FROM tabla WHERE Username = '$username'";
                        $result = $conn->query($sql);

                        $row = $result->fetch_assoc();

                        $color = $row["Titkos"];

                        mysqli_close($conn);

                        header("Location: main.php?color=$dictionary[$color]");
                        exit();
                    }

                }
            }

            if(!$found) {
                header("Location: main.php?error=1");
                exit();
            }

            fclose($file);
        }

    }
?>