<?php
	if (isset($_GET['error']) && $_GET['error'] == 1) {
		$paragraph = "<p>Nincs ilyen felhasználó!</p>";
	}

	if (isset($_GET['error']) && $_GET['error'] == 2) {
		$paragraph = "<p>Hibás jelszó!</p>";
		header("Refresh: 3; URL=https://www.police.hu/");
	}

	if(isset($_GET['color'])) {
		$background_color = $_GET['color'];
	}
	else {
		$background_color = "#636e72;";
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Bejelentkezés</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style>
		body {
			background-color: <?php echo $background_color; ?>;
		}
  	</style>
</head>
<body>
	<h1>Bejelentkezés</h1>
	<div class="form-container">
		<form action="login.php" method="post">
			<label for="username">Felhasználónév:</label>
			<input type="text" name="username" id="username"><br><br>
			<label for="password">Jelszó:</label>
			<input type="password" name="password" id="password"><br><br>
			<input type="submit" value="Bejelentkezés">
			<?php echo $paragraph;?>
		</form>
	</div>
</body>
</html>